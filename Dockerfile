FROM node:12-alpine AS dist

COPY package*.json ./
COPY . .

RUN npm install --silent
RUN npm run build

##############################################################

FROM node:12-alpine
WORKDIR /usr/src/app

COPY .env .
COPY package*.json ./
COPY --from=dist dist /usr/src/app/dist
COPY --from=dist node_modules /usr/src/app/node_modules

EXPOSE $SERVER_PORT

CMD [ "npm", "run", "start" ]
