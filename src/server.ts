import 'dotenv/config';

import system from '../package.json';
import app from './app';
import Logger from './app/lib/logger';

app.listen(process.env.SERVER_PORT, () => {
  Logger.info(`#################################`);
  Logger.info(`## Server running on port ${process.env.SERVER_PORT} ##`);
  Logger.info(`## Version: ${system.version}              ##`);
  Logger.info(`#################################`);
});
