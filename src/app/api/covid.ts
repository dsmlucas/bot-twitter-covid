import axios from 'axios';

const api = axios.create({
  responseType: 'json',
  headers: {},
  auth: {
    username: process.env.API_RND_USER || '',
    password: process.env.API_RND_PASS || '',
  },
});

api.interceptors.request.use(
  config => {
    config.baseURL = `https://${process.env.API_RND_URL}`;

    return config;
  },
  error => Promise.reject(error),
);

api.interceptors.response.use(
  response => response,
  error => Promise.reject(error),
);

export default api;
