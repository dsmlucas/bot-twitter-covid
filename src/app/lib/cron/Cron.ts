import cron from 'node-cron';

import TweetController from '../../controllers/TweetController';

class Cron {
  run() {
    cron.schedule('0 */2 * * *', () => TweetController.run());
  }
}

export default new Cron();
